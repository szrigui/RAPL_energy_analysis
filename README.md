---
output:
  pdf_document: default
  html_document: default
---
# energy_analysis
![](figures/unique_jobs_dist.png)


![](figures/tree_4.png)

    job_runtime: <30: short
    job_runtime: >=30: long:
    
            *test for stationarity:
                  -We divide the signal into n (8 in the used example) equal  windows.
                  -We compute the mean of the variance of each window
                  -We exclude the first and the last window (as they often correspond to an initialization or finishing phase that are  different from the  rest of the signal)
                  -For each signal, we take the mean and the variance of the 6 middle windows.  Then, we compute the mean of the variance of (mean_var), 
                  the variance of the mean  (var_mean) and the variance of the variance (var_var)
            **discrete fourier transform: (test for periodicity)
                  -For each signal, we apply a DFT: the decomposition of a time series into underlying sine and cosine functions of different 
                  frequencies, which allows us to determine the frequencies that appear particularly strong or important.
                  -From that, we obtain a periodogram that quantifies the contributions of the individual frequencies to the signal
                  -We extract the frequency of the highest contribution: Max_spectral_density_freq.

          
          
          
                                